package config

type amqpConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Exchange string
}

type dbConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

type Config struct {
	ManagerName string
	RulesPath   string
	HttpPort    string
	AMQP        amqpConfig
	DB          dbConfig
}

func New() *Config {
	amq := amqpConfig{
		Host:     "localhost",
		Port:     "5672",
		User:     "guest",
		Password: "guest",
		Exchange: "eman",
	}
	db := dbConfig{
		Host:     "localhost",
		Port:     "8529",
		User:     "eman",
		Password: "eman",
		Database: "eman",
	}
	ret := Config{
		ManagerName: "eman-default",
		RulesPath:   "rules",
		HttpPort:    "8080",
		AMQP:        amq,
		DB:          db,
	}
	return &ret
}
