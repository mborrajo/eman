package database

import (
	"fmt"

	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
	"github.com/sirupsen/logrus"
	e "gitlab.com/mborrajo/eman/common"
	c "gitlab.com/mborrajo/eman/eman/config"
)

type DBManager struct {
	logger *logrus.Entry
	DB     driver.Database
	DBColl driver.Collection
}

func (this *DBManager) handleError(err error) {
	if err != nil {
		this.logger.Fatal(err)
	}
}

// Creates new DB Manager
func New(logger *logrus.Logger, cfg *c.Config) (*DBManager, error) {
	dbLogger := logger.WithField("unit", "DBManager")
	manager := DBManager{
		logger: dbLogger,
	}
	manager.logger.Info("Creating new DBManager")

	conn, err := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{fmt.Sprintf("http://%s:%s", cfg.DB.Host, cfg.DB.Port)},
	})
	manager.handleError(err)
	c, err := driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(cfg.DB.User, cfg.DB.Password),
	})
	manager.handleError(err)
	db, err := c.Database(nil, cfg.DB.Database)
	manager.handleError(err)
	manager.DB = db
	exists, err := db.CollectionExists(nil, cfg.ManagerName)
	manager.handleError(err)
	if !exists {
		db.CreateCollection(nil, cfg.ManagerName, nil)
	}
	col, err := db.Collection(nil, cfg.ManagerName)
	manager.handleError(err)
	manager.DBColl = col
	return &manager, nil
}

func (this *DBManager) NewEvent(event *e.Event) {
	event.Key = event.ID
	this.DBColl.CreateDocument(nil, event)
}

/*
func (this *DBManager) Start() {
	//go this.readQueue()
}
*/

func (this *DBManager) FindEvents(condition string) ([]e.Event, error) {
	if condition != "" {
		condition = fmt.Sprintf(" FILTER %s", condition)
	}
	query := fmt.Sprintf("FOR ev IN `%s` %s RETURN ev", this.DBColl.Name(), condition)

	this.logger.WithField("Query", query).Info("FindEvents")

	cursor, err := this.DB.Query(nil, query, nil)
	if err != nil {
		return nil, err
	}
	defer cursor.Close()
	var ret []e.Event
	for {
		var ev e.Event
		_, err := cursor.ReadDocument(nil, &ev)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			this.handleError(err)
		}
		ret = append(ret, ev)
	}
	return ret, nil
}

// SaveEvent Add message to the processing queue
func (this *DBManager) SaveEvent(ev e.Event) {
	this.logger.WithField("EventId", ev.ID).Info("Saving event")
	this.DBColl.UpdateDocument(nil, ev.ID, ev)
}
