cond = [
	"ev.Host == '" + $this.Host + "'",
	"ev.HostGroup == '" + $this.HostGroup + "'",
	"ev.MonitorGroup == '" + $this.MonitorGroup + "'",
	"ev.MonitorClass == '" + $this.MonitorClass + "'",
	"ev.Instance == '" + $this.Instance + "'",
	"ev.Metric == '" + $this.Metric + "'",
	"ev.Origin == '" + $this.Origin + "'",
	"ev.Status != 'CLOSED' "
].join(" AND  ")

FindEvents(cond).forEach(function(e){
	console.log(JSON.stringify(e, null, 2))
	e.Repetitions += 1
	e.MetricValue = $this.MetricValue
	e.Message = $this.Message
	e.Severity = $this.Severity
	e.Status = $this.Status
	UpdateEvent(e)
	$this.Drop = true
})