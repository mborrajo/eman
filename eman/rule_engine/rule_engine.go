package rule_engine

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	e "gitlab.com/mborrajo/eman/common"
	c "gitlab.com/mborrajo/eman/eman/config"
	db "gitlab.com/mborrajo/eman/eman/database"
	"gitlab.com/mborrajo/eman/eman/io"
	vm "gitlab.com/mborrajo/eman/eman/rule_engine/JSVM"

	"github.com/lithammer/shortuuid"
	"github.com/robertkrimen/otto"
	"github.com/sirupsen/logrus"
)

type rule struct {
	filename string
	content  string
}

type RuleEngine struct {
	logger    *logrus.Entry
	jsvm      *vm.JSVM
	dbManager *db.DBManager
	io        *io.EManIO
	//messageInput chan c.Message
	//eventOutput  chan c.Event
	//stopChan     chan bool
	//running      bool
	rules []rule
}

func New(logger *logrus.Logger, cfg *c.Config, dbMan *db.DBManager, emanio *io.EManIO) (*RuleEngine, error) {
	engine := RuleEngine{
		logger: logger.WithField("unit", "RuleEngine"),
		//messageInput: mi,
		//eventOutput:  evOut,
		//stopChan:     make(chan bool),
		//running:      false,
		dbManager: dbMan,
		io:        emanio,
	}
	engine.logger.Info("Creating new RuleEngine")
	engine.jsvm = vm.NewVM()
	engine.ReadJS(cfg.RulesPath)
	engine.initEMFunctions()
	return &engine, nil
}

func (re *RuleEngine) readJSFilesFromFolder(path string, run bool) {
	re.logger.Info(fmt.Sprintf("Reading js files from %s", path))
	filepath.Walk(path, func(fPath string, fInfo os.FileInfo, _ error) error {
		if filepath.Ext(fPath) == ".js" {
			log.Printf("Processing file: %s", fPath)
			byteContent, err := ioutil.ReadFile(fPath)
			if run == true {
				re.jsvm.VM.Run(byteContent)
			}
			if err != nil {
				re.logger.Error(err)
			}
			re.rules = append(re.rules, rule{filename: fPath, content: string(byteContent)})
		}
		return nil
	})
	return
}

func (re *RuleEngine) initEMFunctions() {

	re.jsvm.VM.Set("UpdateEvent", func(call otto.FunctionCall) otto.Value {
		ev := e.Event{}
		vm.VMObjToEvent(call.Argument(0).Object(), &ev)
		re.logger.WithField("Event", ev).Info("ModifyEvent")
		re.dbManager.SaveEvent(ev)
		ret, _ := re.jsvm.VM.ToValue(ev)
		return ret
	})

	re.jsvm.VM.Set("SendEvent", func(call otto.FunctionCall) otto.Value {
		ev := e.Event{}

		dest := call.Argument(0).String()
		vm.VMObjToEvent(call.Argument(1).Object(), &ev)
		re.logger.WithField("Event", ev).Info("SendEvent")
		re.io.SendEvent(dest, ev)
		ret, _ := re.jsvm.VM.ToValue(ev)
		return ret
	})

	re.jsvm.VM.Set("_FindEvents", func(call otto.FunctionCall) otto.Value {
		query := call.Argument(0).String()
		log.Printf("******** Search: %+v", query)
		evs, _ := re.dbManager.FindEvents(query)
		log.Printf("%d Eventos de la bbdd", len(evs))
		ret, _ := re.jsvm.VM.ToValue(evs)
		return ret
	})
	re.jsvm.VM.Run(`
		var FindEvents = function(query) {
			var evs = _FindEvents(query)
			return evs ? evs.map(function(ev) {return JSON.parse(JSON.stringify(ev))}) : []
		}
	`)
}

func (re *RuleEngine) ReadJS(path string) {
	re.readJSFilesFromFolder(path+"/defines", true)
	re.readJSFilesFromFolder(path+"/rules", false)
}

func (re *RuleEngine) ProcessEvent(ev *e.Event) {
	if ev.ID == "" {
		ev.ID = shortuuid.New()
	}

	re.logger.WithField("Message", ev.Message).Info("processMessage")
	re.jsvm.VM.Set("$this", ev)
	for _, rule := range re.rules {
		re.logger.WithField("Rule", rule.filename).Info("Executing Rule")
		_, err := re.jsvm.VM.Run(rule.content)
		if err != nil {
			re.logger.WithField("Rule", rule.filename).Error(err)
		}
	}
	vmRet, err := re.jsvm.VM.Get("$this")
	if err != nil {
		re.logger.WithField("Event", ev).Error("err")
	}
	if vmRet.IsObject() {
		event := e.Event{}
		vm.VMObjToEvent(vmRet.Object(), &event)
		if !event.Drop {
			re.logger.WithField("Event", event).Trace("Save Event")
			re.dbManager.NewEvent(&event)
		} else {
			log.Printf("Droping Event with id: %+v", event.ID)
		}
	}
	return
}

/*
func (re *RuleEngine) Start() {
	re.logger.Info("Starting RuleEngine")
	go func() {
		var stop = false
		re.running = true
		for !stop {
			select {
			case m := <-re.messageInput:
				re.logger.Info("New Message", m)
				re.processMessage(m)
			case <-re.stopChan:
				stop = true
				re.logger.Info("Stopping RuleEngine")
			}
		}
		re.running = false
	}()
}

func (re *RuleEngine) Stop() {
	re.logger.Info("Sending Stop signal")
	re.stopChan <- true
	re.logger.Info("Stop signal sent")
}

func (re *RuleEngine) IsRunning() bool {
	return re.running
}
*/
