package JSVM

import (
	"github.com/robertkrimen/otto"
	_ "github.com/robertkrimen/otto/underscore" // Underscore for the vm
	e "gitlab.com/mborrajo/eman/common"
)

type JSVM struct {
	VM *otto.Otto
}

// Init VM Initializes the js vm
func NewVM() *JSVM {
	vm := otto.New()
	vm.Run(`
		var printAlert = function (alert) {
			console.log("{")
			Object.keys(alert).forEach(function(d){console.log("   " + d + ":" + alert[d])})
			console.log("}")
		}`)
	return &JSVM{
		VM: vm,
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func VMObjToEvent(EventObj *otto.Object, event *e.Event) {
	ID, err := EventObj.Get("ID")
	check(err)
	event.ID, err = ID.ToString()
	check(err)
	Origin, err := EventObj.Get("Origin")
	check(err)
	event.Origin = Origin.String()
	Message, err := EventObj.Get("Message")
	check(err)
	event.Message = Message.String()
	Severity, err := EventObj.Get("Severity")
	check(err)
	event.Severity = Severity.String()
	Status, err := EventObj.Get("Status")
	check(err)
	event.Status = Status.String()
	Host, err := EventObj.Get("Host")
	check(err)
	event.Host = Host.String()
	HostGroup, err := EventObj.Get("HostGroup")
	check(err)
	event.HostGroup = HostGroup.String()
	MonitorGroup, err := EventObj.Get("MonitorGroup")
	check(err)
	event.MonitorGroup = MonitorGroup.String()
	MonitorClass, err := EventObj.Get("MonitorClass")
	check(err)
	event.MonitorClass = MonitorClass.String()
	Instance, err := EventObj.Get("Instance")
	check(err)
	event.Instance = Instance.String()
	Metric, err := EventObj.Get("Metric")
	check(err)
	event.Metric = Metric.String()
	MetricID, err := EventObj.Get("MetricID")
	check(err)
	event.MetricID = MetricID.String()
	Propagations, err := EventObj.Get("Propagations")
	check(err)
	event.Propagations = Propagations.String()
	MetricValue, err := EventObj.Get("MetricValue")
	check(err)
	event.MetricValue, err = MetricValue.ToFloat()
	check(err)
	Drop, err := EventObj.Get("Drop")
	check(err)
	event.Drop, err = Drop.ToBoolean()
	check(err)
}
