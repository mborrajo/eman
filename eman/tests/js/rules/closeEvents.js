var query = {
  Host: $this.Host,
  MonitorClass: $this.MonitorClass,
  Instance: $this.Instance,
  Metric: $this.Metric,
  Status: "OPEN"
}
var evs = FindEvents(query)
if (evs.length > 0) {
	$this.Drop = true
}
evs.forEach(function(ev) {
  //console.log("Alerta de BBDD")
  //printAlert(ev)
  if ($this.Severity === "OK") {
	ev.Status = "CLOSED"  
	ev.Message += " - Evento cerrado por severidad diferente"
  }
  else {
	ev.Message = $this.Message
  }
  UpdateEvent(ev)
})