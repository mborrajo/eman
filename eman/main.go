package main

import (
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/mattn/go-colorable"
	"github.com/sirupsen/logrus"
	e "gitlab.com/mborrajo/eman/common"
	"gitlab.com/mborrajo/eman/eman/config"
	"gitlab.com/mborrajo/eman/eman/database"
	"gitlab.com/mborrajo/eman/eman/io"
	re "gitlab.com/mborrajo/eman/eman/rule_engine"
)

func GetEnvVar(name string, defaultValue string) string {
	aux := os.Getenv(name)
	if aux == "" {
		return defaultValue
	} else {
		return aux
	}
}

func ParseEnv(c *config.Config) *config.Config {
	c.ManagerName = GetEnvVar("EMAN_MANAGER_NAME", c.ManagerName)
	c.RulesPath = GetEnvVar("EMAN_RULES_PATH", c.RulesPath)
	c.HttpPort = GetEnvVar("EMAN_HTTP_PORT", c.HttpPort)

	c.AMQP.Host = GetEnvVar("EMAN_AMQP_HOST", c.AMQP.Host)
	c.AMQP.Port = GetEnvVar("EMAN_AMQP_PORT", c.AMQP.Port)
	c.AMQP.User = GetEnvVar("EMAN_AMQP_USER", c.AMQP.User)
	c.AMQP.Password = GetEnvVar("EMAN_AMQP_PASSWORD", c.AMQP.Password)
	c.AMQP.Exchange = GetEnvVar("EMAN_AMQP_EXCHANGE", c.AMQP.Exchange)

	c.DB.Host = GetEnvVar("EMAN_DB_HOST", c.DB.Host)
	c.DB.Port = GetEnvVar("EMAN_DB_PORT", c.DB.Port)
	c.DB.User = GetEnvVar("EMAN_DB_USER", c.DB.User)
	c.DB.Password = GetEnvVar("EMAN_DB_PASSWORD", c.DB.Password)
	c.DB.Database = GetEnvVar("EMAN_DB_DATABASE", c.DB.Database)

	return c
}

func main() {
	logger := logrus.New()
	logger.SetFormatter(&logrus.TextFormatter{ForceColors: true, FullTimestamp: true})
	logger.SetOutput(colorable.NewColorableStdout())
	logger.Info("Starting EMan")
	cfg := config.New()
	ParseEnv(cfg)

	dbManager, err := database.New(logger, cfg)
	if err != nil {
		logger.Fatal(err)
	}

	emanIO := io.New(cfg)
	input := make(chan e.Event, 100)
	emanIO.Input = input

	ruleEngine, err := re.New(logger, cfg, dbManager, emanIO)

	go func() {
		for ev := range input {
			logger.Info(ev.Message)
			ruleEngine.ProcessEvent(&ev)
		}
	}()
	q := make(chan bool)
	emanIO.Start(q)

	http.Handle("/metrics", promhttp.Handler())
	logger.Info("Starting http server at port " + cfg.HttpPort)
	http.ListenAndServe(":"+cfg.HttpPort, nil)
	q <- true
}
