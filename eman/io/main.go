package io

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/streadway/amqp"
	e "gitlab.com/mborrajo/eman/common"
	"gitlab.com/mborrajo/eman/eman/config"
)

type EManIO struct {
	Input          chan e.Event
	Output         chan e.Event
	AMQPConnection *amqp.Connection
	AMQPChannel    *amqp.Channel
	InputQueue     amqp.Queue
	ExchangeName   string
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func New(c *config.Config) *EManIO {
	ret := EManIO{
		ExchangeName: c.AMQP.Exchange,
	}

	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", c.AMQP.User, c.AMQP.Password, c.AMQP.Host, c.AMQP.Port))
	failOnError(err, "Failed to connect to RabbitMQ")
	ret.AMQPConnection = conn

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	ret.AMQPChannel = ch

	err = ch.ExchangeDeclare(
		c.AMQP.Exchange, // name
		"direct",        // type
		true,            // durable
		false,           // auto-deleted
		false,           // internal
		false,           // no-wait
		nil,             // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.QueueBind(
		q.Name,          // queue name
		c.ManagerName,   // routing key
		c.AMQP.Exchange, // exchange
		false,
		nil,
	)
	failOnError(err, "Failed to bind a queue")
	ret.InputQueue = q

	return &ret
}

func (this *EManIO) SendEvent(destination string, ev e.Event) error {
	body := ev.ToPrettyString()
	err := this.AMQPChannel.Publish(
		this.ExchangeName, // exchange
		destination,       // routing key
		false,             // mandatory
		false,             // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	return err
}
func (this *EManIO) ReadMessage(msg []byte) {
	fmt.Println(string(msg))
	ev := e.Event{}
	json.Unmarshal(msg, &ev)
	this.Input <- ev
}

func (this *EManIO) Start(q chan bool) {
	msgs, err := this.AMQPChannel.Consume(
		this.InputQueue.Name, // queue
		"",                   // consumer
		true,                 // auto-ack
		false,                // exclusive
		false,                // no-local
		false,                // no-wait
		nil,                  // args
	)
	failOnError(err, "Failed to register a consumer")

	go func() {
		for {
			select {
			case m := <-msgs:
				this.ReadMessage(m.Body)
			case _ = <-q:
				this.AMQPChannel.Close()
				this.AMQPConnection.Close()
				return
			}
		}
	}()

}
