package common

import (
	"testing"
)

func Test_New(t *testing.T) {
	msg := "Mensaje"
	t.Log("Test_New")
	ev := NewEvent()
	ev.Message = msg

	if ev.Message != msg {
		t.Errorf("Different message, '%s' : '%s'", ev.Message, msg)
	}
}

/*
func Test_All(t *testing.T) {
	//logger := logrus.New()
	dbm, err := New(logger, nil)
	if err != nil {
		t.Fatalf("Failed to create database: %s", err)
	}

	dbm.Start()

	dbm.SaveEvent(c.Event{
		Host: "Test1",
	})
	time.Sleep(2 * time.Second)
	events, err := dbm.FindEvents("")
	if err != nil {
		t.Fatalf("Unable to retieve events: %s", err)
	}

	for _, ev := range events {
		t.Log(ev.ToPrettyString())
	}
}
*/
